﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseDoorTimer : MonoBehaviour
{
	//Game objects you want to disable
	[SerializeField] GameObject ObjectToActivate;
	[SerializeField] GameObject ObjectToDeActivate;
	/// <summary> Give the amount of seconds you want to get the RNG to choose between. </summary>
	[SerializeField] float NmbrOne, NmbrTwo;

	void OnTriggerEnter(Collider collider)
	{
		//Set a random number between the given times
		float Timer = Random.Range(NmbrOne, NmbrTwo);
		//Convert the timer to seconds and make it work on all refresh rates/fps differences.
		float loopTimer = Timer * (1000 * Time.deltaTime);

		//Wait for the number to appear
		for (int i = 0; i < loopTimer; i++)
		{
			if (collider.tag == "player")
			{
				ObjectToActivate.SetActive(true);
				ObjectToDeActivate.SetActive(false);
				gameObject.SetActive(false);
			}
		}
	}
}