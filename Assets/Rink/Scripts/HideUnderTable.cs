﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideUnderTable : MonoBehaviour
{
	//Variable's
	[SerializeField] GameObject Player;
	[SerializeField] float maxDistance;

	private float PlayerTableDistance;
	
	void Start()
	{
		
	}
	
	void Update()
	{
		//Update the distance between the player and 'THIS' table
		PlayerTableDistance = Vector3.Distance(transform.position, Player.transform.position);
		//Check if the distance between the player and the table are smaller then the given distance.
		if(PlayerTableDistance <= maxDistance)
		{
			Debug.Log("YEET!");
		}
	}
}