﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPlayerDoorDistance : MonoBehaviour
{
	//Variable's
	[SerializeField] GameObject Player;
	[SerializeField] float maxDist;

	private Vector3 door;
	private float Distance;
	
	void Start()
	{
		door = gameObject.transform.position;
		door.z = gameObject.transform.localPosition.x - 0.5f;
	}
	
	void Update()
	{
		Distance = Vector3.Distance(door, Player.transform.position);
		if(Distance < maxDist)
		{
			//Do code
			Debug.Log("Closer then " + maxDist + "m!");
		}
	}
}