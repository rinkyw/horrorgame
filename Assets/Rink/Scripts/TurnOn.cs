﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOn : MonoBehaviour
{
	//Variable's
	[SerializeField] GameObject lols;

	bool done = true;

	void Update()
	{
		if(done == true)
		{
			lols.SetActive(true);
			gameObject.SetActive(false);
			done = false;
		}
	}
}
