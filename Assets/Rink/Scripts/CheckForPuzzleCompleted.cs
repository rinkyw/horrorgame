﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckForPuzzleCompleted : MonoBehaviour
{
	[SerializeField] GameObject Text;
	private bool SolvedPuzzle = false;

	//Check for the player
	public void OnTriggerEnter(Collider collider)
	{
		if (collider.name == "Player")
		{
			Debug.Log("Yeet!!!");
			if(SolvedPuzzle)
			{
				//Code that opens the door
			}
			else
			{
				Text.SetActive(true);
			}
		}
	}
}