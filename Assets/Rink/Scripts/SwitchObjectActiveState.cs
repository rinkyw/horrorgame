﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchObjectActiveState : MonoBehaviour
{
	//Variable's
	[SerializeField] GameObject TurnOn, TurnOff;
	
	void OnTriggerEnter(Collider collider) 
	{
		if(collider.tag == "player")
		{
			TurnOn.SetActive(true);
			TurnOff.SetActive(false);
		}
	}
}
