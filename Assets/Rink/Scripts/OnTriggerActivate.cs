﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// You can only activate this once very nice!.
/// </summary>
public class OnTriggerActivate : MonoBehaviour
{	
	//Variable's
	[SerializeField] GameObject ObjectToActivate;
	
	void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "player")
		{
			ObjectToActivate.SetActive(true);
			gameObject.SetActive(false);
		}
	}
}