﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOffObject : MonoBehaviour
{
	public void TurnOffGameObject()
	{
		//FirstPersonController.Leaning = false;
		gameObject.SetActive(false);
	}
}