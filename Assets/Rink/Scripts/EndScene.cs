﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndScene : MonoBehaviour
{
	//Variable's
	[SerializeField] GameObject endanim;
	
	public void SceneEndAnim()
	{
		endanim.SetActive(true);
	}
}
