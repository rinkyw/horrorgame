﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class TurnCamOnOff : MonoBehaviour
{
	private FirstPersonController firstPersonController = new FirstPersonController();
	[SerializeField] bool LeftOrRightCam; //false = left, true = right

	public void SwitchCams()
	{
		switch (LeftOrRightCam)
		{
			case false:
				firstPersonController.LeftLeaningFalse();
				break;
			case true:
				firstPersonController.RightLeaningFalse();
				break;
		}
		gameObject.SetActive(false);
	}
}