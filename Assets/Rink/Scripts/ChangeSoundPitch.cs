﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSoundPitch : MonoBehaviour
{
	//Variable's
	private AudioSource audioSource;
	
	void Start()
	{
		audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	public void ChangePitch()
	{
		audioSource.pitch = 1;
	}
}