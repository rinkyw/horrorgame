﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateCeiling : MonoBehaviour
{
	//Ceiling game object
	[SerializeField] GameObject Ceiling;
	
	void Start()
	{
		Ceiling.SetActive(true);
	}
}